package me.suzutsuki.Bukkit.PvPDeathMatch;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by Mizuki on 5/1/2014.
 */
public class CommandHandler implements CommandExecutor {

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args){
        if(cmd.getName().equalsIgnoreCase("pvpdm")) {
            if (cs instanceof Player) {
                Player host = (Player)cs;
                if(host.hasPermission("pvpdm.host")){
                    if(args.length != 2) {
                        host.sendMessage(ChatColor.RED + "Usage: /pvpdm <Region> <MaxKill>");
                        host.sendMessage(ChatColor.RED + "The plugin will use item in your hand as a reward");
                        return true;
                    }
                    else {
                        if(PvPDM.enabled){
                            host.sendMessage(ChatColor.RED + "PvP Death Match already started.");
                            return true;
                        }
                        int maxKill = -1;
                        try{
                            maxKill = Integer.parseInt(args[1]);
                        }catch(NumberFormatException e){
                            host.sendMessage(ChatColor.RED + "MaxKill and/or RewardQty param are invalid.");
                            return true;
                        }
                        int rewardQty = (int) Math.floor(maxKill / 10);
                        ItemStack stack = host.getItemInHand().clone();
                        stack.setAmount(rewardQty);
                        if(stack == null){
                            host.sendMessage(ChatColor.RED + "Reward cannot be empty.");
                            return true;
                        }
                        RegionManager rm = WGBukkit.getRegionManager(host.getWorld());
                        String regionId = args[0];
                        ProtectedRegion region = rm.getRegion(regionId);
                        if(region == null){
                            host.sendMessage(ChatColor.RED + "Region not found.");
                            return true;
                        }
                        PvPDM.region = region;
                        PvPDM.maxKill = maxKill;
                        PvPDM.enabled = true;
                        PvPDM.reward = stack;
                        host.sendMessage(ChatColor.RED + "PvPDeathMatch initialized.");
                        Bukkit.broadcastMessage(PvPDM.prefix + "PvP Death Match has started!");
                        Bukkit.broadcastMessage(PvPDM.prefix + "Player who gets " + args[1] + " kills first wins.");
                        return true;
                    }

                }
                else{
                    host.sendMessage(ChatColor.RED + "You don't have permission to do this. I'm very sad. :(");
                    return true;
                }
            } else {
                cs.sendMessage("Console isn't allowed here sorry");
                return true;
            }
        }
        if(cmd.getName().equalsIgnoreCase("pvpkill")){
            if(args.length == 0) {
                if (cs.hasPermission("pvpdm.rank")) {
                    cs.sendMessage(PvPDM.prefix + " Top 10 kills.");
                    TreeMap<Player, Integer> treeMap = new TreeMap<Player, Integer>(new ValueComparator(PvPDM.points));
                    treeMap.putAll(PvPDM.points);
                    int i = 0;
                    for (Player p : treeMap.keySet()) {
                        i++;
                        if (i < 11) {
                            cs.sendMessage(ChatColor.RED + Integer.toString(i) + "." + p.getDisplayName() + ChatColor.RED + " - " + Integer.toString(treeMap.get(p)) + " kills");
                        }
                    }
                    if (cs instanceof Player) {
                        Integer selfKill = treeMap.get((Player) cs);
                        cs.sendMessage(PvPDM.prefix + " Your score is " + (selfKill == null ? "0" : Integer.toString(selfKill)));
                    }
                    return true;
                }
                else
                {
                    cs.sendMessage(ChatColor.RED + "You don't have permission to do this. I'm very sad. :(");
                    return true;
                }
            }
            else if(args.length == 1){
                if(cs.hasPermission("pvpdm.lookup")){
                    Integer score = PvPDM.points.get(Bukkit.getPlayer(args[0]));
                    cs.sendMessage(PvPDM.prefix + args[0] +"'s score is " + (score == null ? "0" : Integer.toString(score)));
                    return true;
                }
                else
                {
                    cs.sendMessage(ChatColor.RED + "You don't have permission to do this. I'm very sad. :(");
                    return true;
                }
            }
        }
        return false;
    }
    class ValueComparator implements Comparator<Player> {

        Map<Player, Integer> base;
        public ValueComparator(Map<Player, Integer> base) {
            this.base = base;
        }

        public int compare(Player a, Player b) {
            if (base.get(a) <= base.get(b)) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}
