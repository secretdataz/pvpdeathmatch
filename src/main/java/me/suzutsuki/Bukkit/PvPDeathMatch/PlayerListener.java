package me.suzutsuki.Bukkit.PvPDeathMatch;

import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Mizuki on 5/1/2014.
 */
public class PlayerListener implements Listener {

    @EventHandler
    public void onKill(PlayerDeathEvent e){
        if(PvPDM.enabled) {
			PvPDM.logger.info("Debug : PvPDM is enabled. test pass");
            Player died = e.getEntity();
            boolean inRegion = false;
            RegionManager rm = WGBukkit.getRegionManager(died.getWorld());
            for(ProtectedRegion rg : rm.getApplicableRegions(died.getLocation())){
                if(PvPDM.region.equals(rg)){
                    inRegion = true;
                }
            }
            if(inRegion){
				PvPDM.logger.info("Debug : Killed in region. test pass");
                EntityDamageEvent damageCause = died.getLastDamageCause();
                if(damageCause.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK || damageCause.getCause() == EntityDamageEvent.DamageCause.PROJECTILE || damageCause.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK || damageCause.getCause() == EntityDamageEvent.DamageCause.FALL){
                    Player killer = died.getKiller();
                    int pts = 1;
                    if(PvPDM.points.containsKey(killer)){
                        pts = PvPDM.points.get(killer) + 1;
                    }
                    PvPDM.points.put(killer,pts);
                    Bukkit.broadcastMessage(PvPDM.prefix + killer.getDisplayName() + " scored " + Integer.toString(pts) + "kills.");
                    if(pts > PvPDM.maxKill - 1){
                        PvPDM.points.clear();
                        PvPDM.enabled = false;
                        Bukkit.broadcastMessage(PvPDM.prefix + killer.getDisplayName() + " got " + Integer.toString(PvPDM.maxKill) + " kills first.");
                        Bukkit.broadcastMessage(PvPDM.prefix + killer.getDisplayName() + " wins the PvP Death Match");
                        killer.getInventory().addItem(PvPDM.reward);
                        PvPDM.reward = null;
                        PvPDM.maxKill = -1;
                    }
                }
            }
        }
    }
}
