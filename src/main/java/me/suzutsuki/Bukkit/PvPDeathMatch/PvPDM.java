package me.suzutsuki.Bukkit.PvPDeathMatch;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.logging.Logger;
import java.util.HashMap;

/**
 * Created by Mizuki on 5/1/2014.
 */
public class PvPDM extends JavaPlugin {

    public static HashMap<Player, Integer> points = new HashMap<Player, Integer>();

    public static ProtectedRegion region = null;
    public static int maxKill = -1;
    public static boolean enabled = false;
    public static ItemStack reward = null;

    public static String prefix = ChatColor.GOLD + "[" + ChatColor.RED + "PvP Manager" + ChatColor.GOLD + "]" + ChatColor.RED;

	public static Logger logger = Logger.getLogger("Minecraft");
	
    public void onEnable(){
        getCommand("pvpdm").setExecutor(new CommandHandler());
		getCommand("pvpkill").setExecutor(new CommandHandler());
        getServer().getPluginManager().registerEvents(new PlayerListener(), this);
    }
}
